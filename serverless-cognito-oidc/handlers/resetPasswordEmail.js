import { log } from './lib/logError';
import { getErrorMessageObject, getSuccessMessageObject } from './lib/responseHelpers';
import { awsForgotPassword } from './lib/awsHelper';

export const resetPasswordEmail = async (event, context, callback) => {
  try {
    const { userEmail } = JSON.parse(event.body);

    if (!userEmail) {
      log('resetPasswordEmail Failed. Required userEmail not submitted in post request');
      return getErrorMessageObject(400, {
        ErrorMessage: 'Invalid Post Request',
        ErrorDetail: 'Required userEmail not submitted in post request'
      });
    }

    try {
      const responseData = await awsForgotPassword(userEmail);
      if (responseData && responseData.CodeDeliveryDetails) {
        return getSuccessMessageObject(200, { message: 'Reset Password email sent successfully' });
      }
    } catch (apiError) {
      log(
        `Reset Password Email CognitoIdentityServiceProvider.forgotPassword returned error. Error Details: ${apiError}`
      );
      return getErrorMessageObject(400, apiError);
    }
  } catch (e) {
    log(`Reset Password Email Failed. Error Details: ${e}`);
    return getErrorMessageObject(400, `Reset Password Email failed: Error Details : ${e}`);
  }
};
