import { log } from './lib/logError';
import { getErrorMessageObject, getSuccessMessageObject } from './lib/responseHelpers';
import { awsDeleteUser } from './lib/awsHelper';

export const deleteUser = async (event, context, callback) => {
  try {
    const { userEmail } = JSON.parse(event.body);

    if (!userEmail) {
      log('deleteUser Failed. Required userEmail not submitted in post request');
      return getErrorMessageObject(400, {
        ErrorMessage: 'Invalid Post Request',
        ErrorDetail: 'Required userEmail not submitted in post request'
      });
    }

    try {
      const responseData = await awsDeleteUser(userEmail);

      if (responseData) {
        return getSuccessMessageObject(200, { message: 'User Deleted successfully' });
      }
    } catch (apiError) {
      log(
        `Delete User CognitoIdentityServiceProvider.adminDeleteUser returned error. Error Details: ${apiError}`
      );
      return getErrorMessageObject(400, apiError);
    }
  } catch (e) {
    log(`Delete User Failed. Error Details: ${e}`);
    return getErrorMessageObject(400, `Delete failed: Error Details : ${e}`);
  }
};
