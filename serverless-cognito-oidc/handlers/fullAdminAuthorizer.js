// Verifies that an access token belongs to a user in the FullAdmin group
import { verifyClaim, generatePolicy } from './lib/claimHelpers';
import { log } from './lib/logError';

export const fullAdminAuthorizer = async (event) => {
  const promise = new Promise(async (resolve, reject) => {
    if (process.env.IS_OFFLINE) {
      const policy = generatePolicy('user', 'allow', event.methodArn);
      reject('Unauthorized');
    }
    try {
      const token = event.authorizationToken;
      await verifyClaim(token);
      const policy = generatePolicy('user', 'allow', event.methodArn);
      resolve(policy);
    } catch (error) {
      log(error);
      reject('Unauthorized');
    }
  });

  return promise;
};
