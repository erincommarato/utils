import reply from './lib/responseHelpers';

export const handler = async (event, context, callback) => {
  return reply(200, {
    message: 'Hello world, you must be logged in as an admin!'
  });
};
