import { awsForgotPassword } from '../lib/awsHelper';
jest.mock('../lib/awsHelper');
const handler = require('../resetPasswordEmail');

describe('resetPasswordEmail', () => {
  it('happy path should return 200', async () => {
    const event = {
      body: JSON.stringify({
        userEmail: 'test@test.com'
      })
    };
    const successResponse = { CodeDeliveryDetails: {} };
    awsForgotPassword.mockImplementation(() => successResponse);
    const response = await handler.resetPasswordEmail(event);

    expect(response.statusCode).toBe(200);
  });
  it('should return 400 when email is missing', async () => {
    const badEvent = {
      body: JSON.stringify({})
    };

    const response = await handler.resetPasswordEmail(badEvent);
    expect(response.statusCode).toBe(400);
    expect(JSON.parse(response.body).errorMessage).toBeTruthy();
  });
});
