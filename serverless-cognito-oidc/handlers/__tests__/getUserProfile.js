import { awsGetUserProfileData } from '../lib/awsHelper';
jest.mock('../lib/awsHelper');
const handler = require('../getUserProfile');

describe('getUserProfile', () => {
  it('happy path should return 200', async () => {
    const event = {
      queryStringParameters: {
        email: 'test@test.com'
      }
    };
    const successResponse = [
      {
        Username: 'e9408d99-88b0-49e4-a929-9554f37f5694',
        Attributes: [
          {
            Name: 'custom:identityId',
            Value: 'us-east-1:f9cb7e31-0b01-4d9a-bb3f-7f8af4bdc237'
          },
          { Name: 'email', Value: 'test@test.com' }
        ],
        UserCreateDate: '2018-11-09T18:12:25.836Z',
        UserLastModifiedDate: '2020-01-09T18:23:36.334Z',
        Enabled: true,
        UserStatus: 'CONFIRMED'
      }
    ];
    awsGetUserProfileData.mockImplementation(() => successResponse);
    const response = await handler.getUserProfile(event);

    expect(response.statusCode).toBe(200);
    expect(JSON.parse(response.body)[0].userEmail).toEqual('test@test.com');
  });
  it('should return 400 when email is missing', async () => {
    const badEvent = {
      body: JSON.stringify({})
    };

    const response = await handler.getUserProfile(badEvent);
    expect(response.statusCode).toBe(400);
    expect(JSON.parse(response.body).errorMessage).toBeTruthy();
  });
});
