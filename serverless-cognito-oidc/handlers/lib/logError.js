/* eslint-disable no-console */
'use strict';

export const log = (message) => {
  console.log(`Error: ${message}`);
  return message;
};
