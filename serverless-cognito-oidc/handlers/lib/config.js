let env = 'local';
if (process.env.APP_ENVIRONMENT === 'test' || process.env.APP_ENVIRONMENT === 'production') {
  env = process.env.APP_ENVIRONMENT;
}

const commonConfigAcrossEnvironments = {
  API_VERSION: '2016-04-18',
  REGION: 'us-east-1'
};

const environmentBasedConfig = {
  local: {
    CLIENT_ID: '1vmlq2la3pncchjjjj9e4rkmbm'
  },
  test: {
    CLIENT_ID: '1vmlq2la3pncchjjjj9e4rkmbm'
  },
  production: {
    CLIENT_ID: '1uk7tie6iehpr85n9a8is068p'
  }
};

const config = { ...commonConfigAcrossEnvironments, ...environmentBasedConfig[env], env };

export default config;
