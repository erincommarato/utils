export const reply = (statusCode, successMessage) => {
  return {
    statusCode: statusCode,
    body: JSON.stringify(successMessage),
    headers: {
      'Cache-Control': 'max-age=0',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true
    }
  };
};
