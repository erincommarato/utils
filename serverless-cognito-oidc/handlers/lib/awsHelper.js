const AWS = require('aws-sdk');
import config from './config';
import { log } from './logError';

const CognitoIdentityServiceProvider = new AWS.CognitoIdentityServiceProvider({
  apiVersion: config.API_VERSION,
  region: config.REGION
});

const getUserPoolId = () => {
  return config.env !== 'production' && config.env !== 'test'
    ? process.env.COGNITO_ACCOUNT_USER_POOL_ID_LOCAL
    : process.env.COGNITO_ACCOUNT_USER_POOL_ID;
};

const awsDeleteUser = async (userEmail) => {
  const params = {
    UserPoolId: getUserPoolId(),
    Username: userEmail
  };
  const apiResponse = await CognitoIdentityServiceProvider.adminDeleteUser(params);
  const responseData = await apiResponse.promise();

  return responseData;
};

const awsForgotPassword = async (userEmail) => {
  const params = {
    ClientId: config.CLIENT_ID,
    Username: userEmail
  };
  const apiResponse = await CognitoIdentityServiceProvider.forgotPassword(params);
  const responseData = await apiResponse.promise();
  return responseData;
};

const awsGetUserProfileData = async (email) => {
  const params = {
    UserPoolId: getUserPoolId(),
    AttributesToGet: ['email', 'custom:identityId'],
    Filter: `email^=\"${email}\"`,
    Limit: 50,
    PaginationToken: null
  };
  let responseData = {};
  try {
    const listOfUsers = await CognitoIdentityServiceProvider.listUsers(params);
    responseData = await listOfUsers.promise();
  } catch (error) {
    responseData.errorMessage = error;
    log(`getUserList failed for ${email}. Error : ${error}`);
  }
  return responseData;
};

export { awsDeleteUser, awsForgotPassword, awsGetUserProfileData };
