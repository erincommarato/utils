import jsonwebtoken from 'jsonwebtoken';
import axios from 'axios';
import jwkToPem from 'jwk-to-pem';
import { promisify } from 'util';

const cognitoIssuer = `https://cognito-idp.us-east-1.amazonaws.com/${process.env.COGNITO_ADMIN_USER_POOL_ID}`;
const verifyPromised = promisify(jsonwebtoken.verify.bind(jsonwebtoken));

const getTokenHeader = (tokenSections) => {
  const headerJSON = Buffer.from(tokenSections[0], 'base64').toString('utf8');
  return JSON.parse(headerJSON);
};

const getPublicKeys = async () => {
  const url = `${cognitoIssuer}/.well-known/jwks.json`;
  const publicKeys = await axios.get(url);
  const keys = publicKeys.data.keys;
  const pems = {};

  keys.forEach((key) => {
    //Convert each key to PEM
    var key_id = key.kid;
    var modulus = key.n;
    var exponent = key.e;
    var key_type = key.kty;
    var jwk = { kty: key_type, n: modulus, e: exponent };
    var pem = jwkToPem(jwk);
    pems[key_id] = pem;
  });

  return pems;
};

const validateClaim = (claim) => {
  const currentSeconds = Math.floor(new Date().valueOf() / 1000);
  if (currentSeconds > claim.exp || currentSeconds < claim.auth_time) {
    throw new Error('claim is expired or invalid');
  }

  if (claim.iss !== cognitoIssuer) {
    throw new Error('claim issuer is invalid');
  }

  if (claim.token_use !== 'access') {
    throw new Error('claim use is not access');
  }

  if (!claim['cognito:groups'] || !claim['cognito:groups'].includes('FullAdmins')) {
    throw new Error('user is not in correct group');
  }
};

const validateKey = (key) => {
  if (key === undefined) {
    throw new Error('claim made for unknown kid');
  }
};

// Helper function to generate an IAM policy
export const generatePolicy = (principalId, effect, resource) => {
  var authResponse = {};

  authResponse.principalId = principalId;
  if (effect && resource) {
    const policyDocument = {};
    const statement = {};
    policyDocument.Version = '2012-10-17';
    policyDocument.Statement = [];
    statement.Action = 'execute-api:Invoke';
    statement.Effect = effect;
    statement.Resource = resource;
    policyDocument.Statement[0] = statement;
    authResponse.policyDocument = policyDocument;
  }
  return authResponse;
};

export const verifyClaim = async (token) => {
  const tokenSections = (token || '').split('.');
  if (tokenSections.length < 2) {
    throw new Error('Requested token is invalid');
  }

  const tokenHeader = getTokenHeader(tokenSections);

  const keys = await getPublicKeys();
  const key = keys[tokenHeader.kid];
  validateKey(key);

  const claim = await verifyPromised(token, key);

  validateClaim(claim);
  return claim;
};
