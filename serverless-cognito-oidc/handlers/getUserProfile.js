import { getErrorMessageObject, getSuccessMessageObject } from './lib/responseHelpers';
import { log } from './lib/logError';
import config from './lib/config';
import { awsGetUserProfileData } from './lib/awsHelper';
const AWS = require('aws-sdk');

const cognitosync = new AWS.CognitoSync({ apiVersion: config.apiVersion });

const formatStatus = (status) => {
  return (
    status &&
    status
      .replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      })
      .replace('_', ' ')
  );
};

const getIdentityPoolId = () => {
  return config.env !== 'production' && config.env !== 'test'
    ? process.env.COGNITO_ACCOUNT_IDENTITY_POOL_ID_LOCAL
    : process.env.COGNITO_ACCOUNT_IDENTITY_POOL_ID;
};

const getObjectFromUserAttributes = (item, attributes) => {
  let returnValue;
  try {
    const returnObject = attributes.filter((attribute) => {
      if (attribute.Name === item) {
        return attribute.Value;
      }
    });
    returnValue = returnObject && returnObject[0].Value;
  } catch (e) {
    log(`ObjectFromAttributes failed for ${item}. Error : ${e}`);
  }
  return returnValue;
};

const getIdentity = async (identityId) => {
  if (!identityId) return null;

  const syncParams = {
    DatasetName: 'userData',
    IdentityId: identityId,
    IdentityPoolId: getIdentityPoolId()
  };

  let responseData = null;
  try {
    const apiResponse = await cognitosync.listRecords(syncParams);
    responseData = await apiResponse.promise();
  } catch (e) {
    log(`getIdentity failed for ${identityId} because ${e}`);
  }

  return responseData;
};

const mapUserList = async (response) => {
  return Promise.all(
    response.map(async (user) => {
      const oneUser = {};
      try {
        oneUser.userEmail = getObjectFromUserAttributes('email', user.Attributes);
        oneUser.userCreateDate = user.UserCreateDate;
        oneUser.enabled = user.Enabled;
        oneUser.userStatus = formatStatus(user.UserStatus);
        oneUser.userProfileLastModifiedDate = user.UserLastModifiedDate;
        oneUser.identityId = getObjectFromUserAttributes('custom:identityId', user.Attributes);

        const identityData = await getIdentity(oneUser.identityId);
        if (identityData && identityData.Records && identityData.Records.length > 0) {
          const parsedUserData = JSON.parse(identityData.Records[0].Value);
          if (parsedUserData && parsedUserData.UserProfile) {
            oneUser.userInfo = parsedUserData.UserProfile.User;
            oneUser.userProfileLastModifiedDate = identityData.Records[0].LastModifiedDate;
            oneUser.numberOfDoctors =
              parsedUserData.UserProfile.Providers && parsedUserData.UserProfile.Providers.length;
            oneUser.syncCount = identityData.Records[0].SyncCount;
          }
        }
      } catch (error) {
        log(`Mapping failed for ${oneUser.userEmail}: ${error}`);
      }
      return oneUser;
    })
  );
};

export const getUserProfile = async (event, context, callback) => {
  try {
    const userEmail = event.queryStringParameters && event.queryStringParameters.email;

    if (!userEmail) {
      log('getUserProfile Failed. Required userEmail not submitted in get request');
      return getErrorMessageObject(400, {
        ErrorMessage: 'Invalid get Request',
        ErrorDetail: 'Required userEmail not submitted in get request'
      });
    }

    const userProfileData = await awsGetUserProfileData(userEmail);
    if (userProfileData.errorMessage) {
      return getErrorMessageObject(400, userProfileData.errorMessage, true);
    }

    const allUsers = await mapUserList(userProfileData.Users);

    return getSuccessMessageObject(200, allUsers);
  } catch (error) {
    return getErrorMessageObject(400, error, true);
  }
};
