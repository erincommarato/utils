import React from 'react';
import useAuth from '../../lib/useAuth';
import auth from '../../lib/auth';
import './header.less';

const Header = () => {
  const { isLoggedIn } = useAuth();

  const handleSignIn = () => {
    auth.signin();
  };

  const handleSignOut = () => {
    auth.signout();
  };

  return (
    <div className="header__container">
      {isLoggedIn ? (
        <button
          className="header__button link"
          data-qa-target="logout-button"
          onClick={handleSignOut}>
          Sign Out
        </button>
      ) : (
        <button
          className="header__button link"
          data-qa-target="login-button"
          onClick={handleSignIn}>
          Sign In
        </button>
      )}
    </div>
  );
};

export default Header;
