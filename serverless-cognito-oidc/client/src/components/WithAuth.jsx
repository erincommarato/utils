import React from 'react';
import NotLoggedIn from './NotLoggedIn';
import useAuth from '../lib/useAuth';

const withAuth = WrappedComponent => {
  return props => {
    const { user, isLoggedIn } = useAuth();
    return isLoggedIn ? <WrappedComponent {...props} user={user} /> : <NotLoggedIn />;
  };
};

export default withAuth;
