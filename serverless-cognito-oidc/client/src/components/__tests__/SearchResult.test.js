import * as React from 'react';
import { shallow } from 'enzyme';
import SearchResult from '../SearchResult';
import mockUserData from '../mocks/mockUserData';

jest.mock('oidc-client', () => ({
  UserManager: () => ({
    getUser: jest.fn(),
    signin: jest.fn(),
    signout: jest.fn(),
    processSignin: jest.fn()
  })
}));

describe('Search Results', () => {
  const oneUser = mockUserData[0];
  const wrapper = shallow(<SearchResult {...oneUser} />);

  it('should display details for one user', () => {
    expect(wrapper).toBeTruthy();
    expect(wrapper.find('.search-result')).toHaveLength(1);
    expect(wrapper.find('.search-result__status--value')).toHaveLength(1);
    expect(wrapper.find('.search-result__status--value').text()).toContain('Enabled');
    expect(wrapper.find('.search-result__grid').find('div').first().text()).toContain(
      'user1@mailinator.com'
    );
  });

  it('should have two buttons', () => {
    expect(wrapper.find('.search-result__button-holder')).toHaveLength(1);
    expect(wrapper.find('.search-result__button')).toHaveLength(2);
    expect(wrapper.find('.search-result__button').first().text()).toContain(
      "Send 'reset password' email"
    );
  });

  it('should show the reset password modal', () => {
    wrapper.find('.search-result__button--reset').simulate('click');
    expect(wrapper.find('.search-result__modal-container')).toHaveLength(1);
    expect(wrapper.find('.search-result__modal-button')).toHaveLength(2);
  });
});
