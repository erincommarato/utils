import React from 'react';
import { shallow } from 'enzyme';
import SearchResults from '../SearchResults';
import mockUserData from '../mocks/mockUserData';

jest.mock('oidc-client', () => ({
  UserManager: () => ({
    getUser: jest.fn(),
    signin: jest.fn(),
    signout: jest.fn(),
    processSignin: jest.fn()
  })
}));

describe('Search Results', () => {
  it('should display two users', () => {
    const query = 'user';
    const wrapper = shallow(<SearchResults query={query} results={mockUserData} />);

    expect(wrapper).toBeTruthy();
    expect(wrapper.find('.search-results__list')).toHaveLength(1);
    expect(wrapper.find('.search-results__list').children()).toHaveLength(2);
  });

  it('should display a message when no results are found', () => {
    const query = 'none';
    const wrapper = shallow(<SearchResults query={query} results={[]} />);

    expect(wrapper).toBeTruthy();
    expect(wrapper.find('.search-result--empty')).toHaveLength(1);
    expect(wrapper.find('.search-result--empty').text()).toContain(
      'No results found for that email address.'
    );
  });
});
