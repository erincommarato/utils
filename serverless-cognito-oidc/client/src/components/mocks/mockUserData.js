export default [
  {
    userEmail: 'user1@mailinator.com',
    userFullName: 'User Name',
    userFullAddress: '123 Street, Denver, CO 80202',
    registrationStatus: true,
    userStatus: 'Enabled',
    identityId: 'username123',
    createdAtDate: '2020-02-04T10:00:00',
    updatedAtDate: '2020-03-11T10:00:00',
    savedProviders: [{ a: 1 }, { b: 2 }],
    profileEdits: 1
  },
  {
    userEmail: 'user2@mailinator.com',
    userFullName: 'User Name 2',
    userFullAddress: '345 Street, Boulder, CO 80202',
    registrationStatus: true,
    userStatus: 'Pending',
    identityId: 'username345',
    createdAtDate: '2020-02-04T10:00:00',
    updatedAtDate: '2020-03-11T10:00:00',
    savedProviders: [{ a: 1 }, { b: 2 }],
    profileEdits: 2
  }
];
