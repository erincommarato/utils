import React from 'react';
import { storiesOf } from '@storybook/react';
import './storybookStyles';

import Search from '../Search';
import App from '../../App';

storiesOf('Account Admin Dashboard/Mock Data', module)
  .add('App', () => <App />)
  .add('Search With Results', () => <Search />);
