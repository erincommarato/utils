/* eslint-disable camelcase */
import apiUrl from './lib/apiUrl';
import envName from './lib/getEnv';

class DummyResponseValidator {
  /**
   * Passes through response from cognito which includes id_token
   * This normally throws an error with oidc-client-js
   * See workaround at https://github.com/IdentityModel/oidc-client-js/issues/645
   */
  validateSigninResponse(state, response) {
    return response;
  }
}

const awsConfig = {
  local: {
    authority: 'https://account-admin-test.auth.us-east-1.amazoncognito.com/login',
    client_id: '2knrsc9hg5c15418q21eu8e0t6',
    redirect_uri: 'http://localhost:8081/',
    post_logout_redirect_uri: 'http://localhost:8081/',
    response_type: 'token',
    scope: 'openid',
    automaticSilentRenew: true,
    metadata: {
      issuer: 'https://cognito-idp.us-east-1.amazonaws.com/us-east-1_dNqHE3P39',
      jwks_uri:
        'https://cognito-idp.us-east-1.amazonaws.com/us-east-1_dNqHE3P39/.well-known/jwks.json',
      authorization_endpoint:
        'https://account-admin-test.auth.us-east-1.amazoncognito.com/oauth2/authorize',
      token_endpoint: 'https://account-admin-test.auth.us-east-1.amazoncognito.com/oauth2/token',
      userinfo_endpoint:
        'https://account-admin-test.auth.us-east-1.amazoncognito.com/oauth2/userInfo',
      end_session_endpoint: `https://account-admin-test.auth.us-east-1.amazoncognito.com/logout?client_id=2knrsc9hg5c15418q21eu8e0t6&logout_uri=http://localhost:8081/`
    },
    ResponseValidatorCtor: DummyResponseValidator
  },
  test: {
    authority: 'https://account-admin-test.auth.us-east-1.amazoncognito.com/login',
    client_id: '2knrsc9hg5c15418q21eu8e0t6',
    redirect_uri: 'https://testaws.healthgrades.com/account-admin/',
    post_logout_redirect_uri: 'https://testaws.healthgrades.com/account-admin/',
    response_type: 'token',
    scope: 'openid',
    automaticSilentRenew: true,
    metadata: {
      issuer: 'https://cognito-idp.us-east-1.amazonaws.com/us-east-1_dNqHE3P39',
      jwks_uri:
        'https://cognito-idp.us-east-1.amazonaws.com/us-east-1_dNqHE3P39/.well-known/jwks.json',
      authorization_endpoint:
        'https://account-admin-test.auth.us-east-1.amazoncognito.com/oauth2/authorize',
      token_endpoint: 'https://account-admin-test.auth.us-east-1.amazoncognito.com/oauth2/token',
      userinfo_endpoint:
        'https://account-admin-test.auth.us-east-1.amazoncognito.com/oauth2/userInfo',
      end_session_endpoint: `https://account-admin-test.auth.us-east-1.amazoncognito.com/logout?client_id=2knrsc9hg5c15418q21eu8e0t6&logout_uri=https://testaws.healthgrades.com/account-admin/`
    },
    ResponseValidatorCtor: DummyResponseValidator
  },
  production: {
    authority: 'https://account-admin-production.auth.us-east-1.amazoncognito.com/login',
    client_id: '4hm5von1mih4golb0sgblilr1o',
    redirect_uri: 'https://www.healthgrades.com/account-admin/',
    post_logout_redirect_uri: 'https://www.healthgrades.com/account-admin/',
    response_type: 'token',
    scope: 'openid',
    automaticSilentRenew: true,
    metadata: {
      issuer: 'https://cognito-idp.us-east-1.amazonaws.com/us-east-1_9dz9HsOUf',
      jwks_uri:
        'https://cognito-idp.us-east-1.amazonaws.com/us-east-1_9dz9HsOUf/.well-known/jwks.json',
      authorization_endpoint:
        'https://account-admin-production.auth.us-east-1.amazoncognito.com/oauth2/authorize',
      token_endpoint:
        'https://account-admin-production.auth.us-east-1.amazoncognito.com/oauth2/token',
      userinfo_endpoint:
        'https://account-admin-production.auth.us-east-1.amazoncognito.com/oauth2/userInfo',
      end_session_endpoint:
        'https://account-admin-production.auth.us-east-1.amazoncognito.com/logout?client_id=4hm5von1mih4golb0sgblilr1o&logout_uri=https://www.healthgrades.com/account-admin/'
    },
    ResponseValidatorCtor: DummyResponseValidator
  }
};

export default awsConfig[envName];

export const appConfig = {
  redirectUrl: awsConfig[envName].redirect_uri,
  apiUrl
};
