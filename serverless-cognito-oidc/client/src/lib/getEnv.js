const getClientEnvironment = () => {
  if (typeof window !== 'undefined' && window.location.hostname.includes('testaws.healthgrades')) {
    return 'test';
  } else if (
    typeof window !== 'undefined' &&
    window.location.hostname.includes('www.healthgrades')
  ) {
    return 'production';
  }
  return 'local';
};

export default getClientEnvironment();
