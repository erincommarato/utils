const logError = error => {
  const errorText = error ? error.toString() : 'no error available';

  const payload = {
    logLevel: 'Error',
    logMessage: errorText,
    messageType: 'adminDashboardError'
  };
  fetch('/api3/logging', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
  }).catch(error => {
    /* eslint-disable no-console */
    console.log('error attempting to log error: ' + error);
    /* eslint-enable no-console */
  });
};

export default logError;
