let hostName = '/';

if (typeof window !== 'undefined') {
  const origin = window.location.origin;
  if (origin.includes('localhost')) {
    hostName = 'http://localhost:3000';
  } else if (origin.includes('testaws')) {
    hostName = 'https://apigwdev.healthgrades.com/account-admin';
  } else {
    hostName = 'https://apigw.healthgrades.com/account-admin';
  }
}

export default hostName;
