import { useState, useEffect } from 'react';
import auth from './auth';
import { appConfig } from '../config';
import logError from './logError';

function useAuth() {
  const [user, setUser] = useState({});
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    const getLoggedInUser = async () => {
      const user = await auth.getUser();
      setUser(user);
      setIsLoggedIn(!!user);
    };
    const processSignin = async () => {
      let hash = new URL(document.location).hash;

      if (hash) {
        try {
          await auth.processSignin();
          setIsLoggedIn(true);
          window.location.assign(appConfig.redirectUrl);
        } catch (e) {
          logError(`${e.toString()}: ${e.componentStack}`);
          console.log('e', e);
        }
      }
    };
    processSignin();
    getLoggedInUser();
  }, []);

  return {
    user,
    isLoggedIn
  };
}

export default useAuth;
