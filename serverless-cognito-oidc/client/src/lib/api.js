import { appConfig } from '../config';
import auth from './auth';
import logError from './logError';

const api = async (resource, options) => {
  const user = await auth.getUser();
  return fetch(`${appConfig.apiUrl}/${resource}`, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: user.access_token
    },
    ...options
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error(
          `Failed to fetch url ${appConfig.apiUrl}/${resource}. Response code was ${response.status}.`
        );
      }
      return response.json();
    })
    .catch((error) => {
      logError(error);
      console.log(`Error when attempting to fetch resource ${resource}:`, error);
      return { errorMessage: error };
    });
};

const get = (resource) => api(resource);
const post = (resource, content) =>
  api(resource, { method: 'POST', body: JSON.stringify(content) });
const put = (resource, content) => api(resource, { method: 'PUT', body: JSON.stringify(content) });
const remove = (resource, content) =>
  api(resource, { method: 'DELETE', body: JSON.stringify(content) });

export const postResetPasswordEmail = (params) => post('resetPasswordEmail', params);
export const postDeleteUser = (params) => post('deleteUser', params);
export const getUserProfile = (searchQuery) => get(`getUserProfile?email=${searchQuery}`);
