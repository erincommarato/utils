import { UserManager } from 'oidc-client';
import config from '../config';

const userManager = new UserManager(config);

const auth = {
  getUser: () => userManager.getUser().then(data => data),
  signin: () => userManager.signinRedirect(),
  signout: () => userManager.signoutRedirect(),
  processSignin: () => userManager.signinRedirectCallback().then(data => data)
};

export default auth;
