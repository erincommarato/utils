const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = (env = {}, argv) => {
  const isProdBuild = argv.mode === 'production';
  return {
    entry: './src/index.js',
    mode: isProdBuild ? 'production' : 'development',
    module: {
      rules: [
        {
          test: /\.(js|jsx|es)$/,
          exclude: /node_modules/,
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-react', '@babel/preset-env'],
            plugins: [
              '@babel/plugin-transform-runtime',
              '@babel/plugin-proposal-class-properties',
              '@babel/plugin-proposal-object-rest-spread',
              '@babel/plugin-syntax-dynamic-import',
              '@babel/plugin-transform-modules-commonjs'
            ]
          }
        },
        {
          test: /\.(js|jsx|es)$/,
          include: [/node_modules[\/\\]\@hg[\/\\]/, /node_modules[\/\\]\joy[\/\\]/],
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react'],
            plugins: [
              '@babel/plugin-transform-runtime',
              '@babel/plugin-proposal-class-properties',
              '@babel/plugin-proposal-object-rest-spread',
              '@babel/plugin-syntax-dynamic-import',
              '@babel/plugin-transform-modules-commonjs'
            ]
          }
        },
        {
          test: /\.mo\.less$/,
          use: [
            {
              loader: 'style-loader'
            },
            {
              loader: 'css-loader',
              options: {
                modules: true,
                importLoaders: 1,
                localIdentName: '[name]_[local]-[hash:base64:5]'
              }
            },
            {
              loader: 'less-loader'
            }
          ]
        },
        {
          test: /\.less$/,
          exclude: /\.mo\.less$/,
          loaders: ['style-loader', 'css-loader', 'less-loader']
        },
        {
          test: /\.css$/,
          use: [{ loader: 'style-loader' }, { loader: 'css-loader' }]
        },
        {
          test: /\.(png|svg|jpg|gif)$/,
          use: [{ loader: 'file-loader?name=/img/[name].[ext]' }]
        }
      ]
    },
    resolve: { extensions: ['*', '.js', '.jsx', '.less', '.md', '.es'] },
    output: {
      path: path.resolve(__dirname, 'public/'),
      publicPath: '/public/',
      filename: 'bundle.js'
    },
    devServer: {
      contentBase: path.join(__dirname, 'public/'),
      port: 8081,
      publicPath: 'http://localhost:8081/',
      hotOnly: true,
      historyApiFallback: true
    }
  };
};
