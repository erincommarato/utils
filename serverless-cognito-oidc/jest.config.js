// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
  transform: {
    '^.+\\.jsx$': 'babel-jest',
    '^.+\\.js$': 'babel-jest',
    '^.+\\.es$': 'babel-jest',
    '^.+\\.(css|less)$': './client/src/components/mocks/styleMock.js'
  },
  // Indicates whether the coverage information should be collected while executing the test
  collectCoverage: true,

  // The directory where Jest should output its coverage files
  coverageDirectory: '.test-coverage',

  // An array of regexp pattern strings used to skip coverage collection
  coveragePathIgnorePatterns: ['/node_modules/'],

  // A list of reporter names that Jest uses when writing coverage reports
  coverageReporters: [
    'json',
    'html'
    //"text",
    //"lcov",
    //"clover"
  ],
  moduleNameMapper: { '@hg/joy/(.*)$': '@hg/joy/$1' },
  resetMocks: true,
  setupFiles: ['dotenv/config'],
  setupFilesAfterEnv: ['./enzyme.setup.js'],
  // The test environment that will be used for testing
  testEnvironment: 'node',
  testPathIgnorePatterns: ['<rootDir>/oldSrc/'],
  transformIgnorePatterns: ['node_modules/(?!(@hg|joy)/)']
};
