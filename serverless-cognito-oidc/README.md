# What is it? 

This is the repository for Account Admin Dashboard. It is a serverless/Cloudformation stack that uses API Gateway backed by Lambda functions and authenticated by Cognito. It's main purpose is to enable the management of user accounts on healthgrades.com

# Security

endpoints are protected at the API Gateway layer through Cognito User Pools and a custom authorizer. All endpoints are accessible only through a Cognito access token from the account-admin user pool in test and prod. The `delete user` endpoint is only available if that cognito user is also part of the `FullAdmins` group within the account-admin cognito user pool.

# Dev Dependencies 

- Node v10.15+
- NPM v6.13+
 
# Dev Setup

- Clone this repo
- Update .env with your variables
- `npm install`
- `npm start`

## Env Variables by Environment
By default, the serverless env plugin looks for the file: .env. In most use cases this is all that is needed. However, there are times where you want different env files based on environment. For instance:

.env.development
.env.production
When you deploy with NODE_ENV set: NODE_ENV=production sls deploy the plugin will look for a file named .env.production. If it doesn't exist it will default to .env.

## Cognito User

- Sign into AWS 
- Go to Cognito
- Go to account-admin-{env}
- Create a user and assign it to the "Full Admins" group for full permissions

# Build & Deploy Process



 

