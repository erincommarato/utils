function checkImageExists(imageUrl, callBack) {
  var imageData = new Image();
  imageData.onload = function() {
    callBack(true);
  };
  imageData.onerror = function() {
    callBack(false);
  };
  imageData.src = imageUrl;
}

// image url that want to check
var imageFile = 'http://example.com/image.png';

// Here pass image url like imageFile in function to check image exist or not.

checkImageExists(imageFile, function(existsImage) {
  if (existsImage == true) {
    // image exist
    console.log('exists');
  } else {
    console.log('does not exist');
    // image not exist
  }
});
