#!/bin/bash
accountId=$(aws sts get-caller-identity --query "Account" --output text)
for region in `aws ec2 describe-regions --output text | cut -f4`; do
  echo -e "\nListing Instances in region:'$region'..."
  instances=$(aws ec2 describe-instances --query "Reservations[*].Instances[*].{IP:PublicIpAddress,ID:InstanceId,Type:InstanceType,State:State.Name,Name:Tags[?Key=='Name'].Value,Platform:Platform,AvailabilityZone:Placement.AvailabilityZone}" --region $region)
  echo $instances | jq -r --arg region "$region" --arg accountId "$accountId" '. | flatten | .[] | [.ID, .Name[], .Platform, .State, $region, $accountId]'
done
